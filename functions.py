from requests import get,exceptions
from re import finditer

# Module functions
# This module defines the functions used in the scraper

# This function obtains the HTML from an URL
def get_html_source(url):
	try:
		html=get(url).text
	except exceptions.ConnectionError:
		html=""
	return html
		
# This function will return a list with all the tags in any HTML string
def get_tags_from_string(html):
	# RegExp to read every HTML tag in the source
	tagsExplorer="(?i)<(\w+)((\s+\w+(\s*=\s*(?:\".*?\"|'.*?'|[^'\">\s]+))?)+\s*|\s*)\/?>"
	
	tags=[]
	# "match" will have each occurence of the found HTML tags
	# once finditer parses the HTML string passed on
	for match in finditer(tagsExplorer,html):
		# We need to get only the tag part. This is "div", "li", "ul", etc. match.group(0) will have the full
		# text line parsed, i.e. <div class="style" id="an-element">, while match.group(1) will containg just "div"
		tag=match.group(1)
		# We append the tag to the list. the use of str() is to convert the string from unicode to plain string
		# avoiding the u'' format
		tags.append(str(tag))
	return tags
	
