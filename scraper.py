#!/usr/bin/python2.7
# encoding: utf8

from collections import Counter
from functions import *

# Initialization of variables. 
totalHtml=0
htmlList=[]


# Reads the provided URL, getting the content
html=get_html_source('http://ordergroove.com/company')

# get the tags from the HTML source 
tags=get_tags_from_string(html)

# mystring will act as a placeholder to build the needed output.
# In this case, which first item is the count of HTML tags converted as a string
mystring=["%s" % (len(tags))]

# We get the 5 most used tags, using the Counter collection, applied to the "tags" list
for i in Counter(tags).most_common(5):
	# For each of the tags, we append it to mystring, in the format tag:count
	# each i element will be in the format ('string',count)
    mystring.append("%s:%s" % i)

# Output the metrics, joining every string in mystring using commas
print(",".join(mystring))

